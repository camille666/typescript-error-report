interface Option {
    api: string;
    project: string;
    error: {
        log: boolean;
        alert: boolean;
        limit: 2;
    };
    onBeforeSend(data: any): any;
}
export default class ErrorReporter {
    option: Option;
    constructor(option: Option);
    _addEvent(): void;
    report: (data: any, isAlert?: boolean) => void;
}
export {};
